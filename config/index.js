const config =  require("./config.production");

if (process.env.NODE_ENV === "development") {
    try {
        for (let [name, value] of Object.entries(require("./config.development"))) {
          config[name] = value;
        }
    } catch(err) {
        console.warn('[dev-config-missing]', err);
    }
}

module.exports = config;
